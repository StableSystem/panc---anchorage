# PANC - Anchorage Ted Stevens International by Zero Dollar Payware

For brevity's sake and so I don't have to update multiple sources all information regarding this project is available on the official forum thread

**[X-PLANE.ORG POST](https://forums.x-plane.org/index.php?/files/file/61336-panc-anchorage-ted-stevens-international-by-zero-dollar-payware/)**

**[OFFICIAL FORUM THREAD](https://forums.x-plane.org/index.php?/forums/topic/190147-panc-anchorage-ted-stevens-intl-by-zero-dollar-payware/#comments)**

**Required Libraries**
* MisterX Library
* SAM Library

Please report any bugs on the official project discord or on the official forum thread (linked above)

To conctact me about this repo please send me a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-stablesystem/)

This repository and it's contents are protected under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
Assets used from other developers was done so with their knowledge and approval. 

**Credits**

Modeling of 30+ buildings by Alex Siu

Orthophotos courtesy of [USGS](https://earthexplorer.usgs.gov/).

Original WED scenery by Iced8383

SAM marshaller configuration by christianh727

**Open Source Resources**

This scenery is entirely open source and I encourage people to look through my source if you are curious how something is done or want to make modifications. If you modify something and think it should be implemented, please send me a message and I'd be happy to merge it into the official release. 

PANC full source - https://gitlab.com/StableSystem/panc---anchorage

Custom Asset source - https://gitlab.com/StableSystem/asset-source

